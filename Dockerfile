FROM arm64v8/python:3.9
RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENTRYPOINT [ "python" ]
CMD ["./app.py" ]