# 1. Usage Local
Installation :
```
sudo pip install RPi.GPIO==0.7.1a4
sudo pip install rpi_ws281x adafruit-circuitpython-neopixel
sudo pip install adafruit-blinka
sudo pip install flask
sudo pip install python-dotenv
sudo pip install -U flask-cors

```
Usage :
```
sudo flask run --host=0.0.0.0 --port=5000
```
<!-- 
# 2. Avec Docker
Construire l'image :
```
docker build -t led-api:latest .
```
Lancer le container
```
sudo docker run --device /dev/gpiomem -d -p 5000:5000 led-api
``` -->

# Homebridge
sudo npm install -g --unsafe-perm homebridge homebridge-config-ui-x
sudo hb-service install --user homebridge



# npm link dans dossier plugin 
# pwd dans dossier plugin
npm root #=> go
ls /usr/lib/node_modules/homebridge
sudo npm install /home/manjaro/Projects/5MOC/LED_API/plugin
/var/lib/homebridge ????

```
{
    "bridge": {
        "name": "Homebridge B32B",
        "username": "0E:54:4B:C1:B3:2B",
        "port": 51779,
        "pin": "991-34-858"
    },
    "accessories": [
        {
            "name": "MY LED",
            "url": "https://raspberry-led.herokuapp.com",
            "accessory": "homebridge-esgiled.LED"
        }
    ],
    "platforms": [
        {
            "name": "Config",
            "port": 8581,
            "platform": "config"
        }
    ]
}

```

# Heroku
https://stackabuse.com/deploying-a-flask-application-to-heroku/

# Install plugin :
``` homebridge-esgiled```