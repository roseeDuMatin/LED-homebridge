import json
import string
import board
import neopixel
import time
import os
from dotenv import load_dotenv
from flask import Flask, request, jsonify
from flask_cors import CORS

load_dotenv()

GUID = os.getenv('GUID')
HOMEBRIDGE_URL = os.getenv('HOMEBRIDGE_URL')

class Ruban():
    def __init__(self, pin, led_number: int):
        self.pixels = neopixel.NeoPixel(pin, led_number)
        self.led_number = led_number
        self.colors = []

    def fill(self, color: list[int, int, int], name):
        self.set_Color(color, name)
        return

    def power_on(self):
        self.set_Color(WHITE, "WHITE")
        return

    def power_off(self):
        self.set_Color(BLANK, "BLANK")
        return

    def multiple_fill(self, colors: list[list[int, int, int]], names: list[str]):
        for i in range(len(colors)):
            time.sleep(0.5)
            self.pixels.fill(colors[i])
            time.sleep(0.5)
            self.pixels.fill(BLANK)
        self.colors = names

    def get_Color(self):
        return self.colors

    def set_Color(self, value: list[int, int, int], name):
        self.pixels.fill(value)
        self.colors = [name]
        return

PIN = board.D18
LED_NUMBER = 60

WHITE, RED, GREEN, BLUE, BLANK, YELLOW, PURPLE, PINK, ORANGE = (255, 255, 255), (255, 0, 0), (0, 255, 0), (0, 0, 255), (0, 0, 0), (255, 255, 0), (75,0,130), (255,0,255), (255,69,0)
COLORS = {
    "BLANK": BLANK,
    "BLUE": BLUE,
    "GREEN": GREEN,
    "RED": RED,
    "WHITE": WHITE,
    "YELLOW": YELLOW,
    "PURPLE": PURPLE,
    "PINK": PINK,
    "ORANGE": ORANGE
}

app = Flask(__name__)
CORS(app)

ruban = Ruban(PIN, LED_NUMBER)

@app.route("/")
def root():
    result = {
        "name": "LED_STRIP",
        "guid": GUID,
        "colors": ruban.get_Color(),
    }
    return jsonify(result=result)

@app.route("/fill/<string:name>")
def fill(name: string):
    errors = []
    name = name.upper()

    keys = COLORS.keys()

    if name not in keys:
        errors.append(f"'{name}' is not a known color")
        return jsonify(result=None, errors=errors)

    color = COLORS[name]

    ruban.fill(color, name)
    return jsonify(result=name, errors=errors)

# @app.route("/test/<int:red>/<int:green>/<int:blue>")
# def test(red: int, green:int, blue: int):
#     color = [ red, green, blue]
#     ruban.colors(color)
#     return jsonify(result=color)
    

@app.route("/array_fill", methods=['POST'])
def array_fill():
    errors = []

    content = request.json
    if (content is None):
        errors.append(f"No content")
        return jsonify(result=None, errors=errors)


    contentKeys = content.keys()
    if "colors" not in contentKeys:
        errors.append(f"Json must have 'colors' field")
        return jsonify(result=None, errors=errors)

    names = content["colors"]
    if type(names) is not list:
        errors.append(f"<p>'colors' field must be an array</p>")
        return jsonify(result=None, errors=errors)

    colorKeys = COLORS.keys()
    result = []
    colors = []
    for index in range(len(names)):
        if type(names[index]) is not str:
            errors.append(f"'{names[index]}' is not a string")
            continue

        name = names[index].upper()
        if name not in colorKeys:
            errors.append(f"'{name}' is not a known color")
            continue

        result.append(name)
        colors.append(COLORS[name])
    
    ruban.multiple_fill(colors, result)
    return jsonify(result=result, errors=errors)

@app.route("/off")
def off():
    ruban.power_off()
    return jsonify(result=True, errors=[])

@app.route("/on")
def on():
    ruban.power_on()
    return jsonify(result=True, errors=[])

# @app.route("/status")
# def status():
#     result = ruban.isOn()
#     return jsonify(result=result, errors=[])